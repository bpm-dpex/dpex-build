
# dpex-build

This git repository organizes the build process of a dpex application.
The repository specifies related repositories, in which the source code of dpex modules is organized, as git submodules.

The ``dpex-build`` repository assists in assembling the dpex modules into a runnable docker image.

## Usage

Please execute the following commands to build and run the dpex application.

 1. Clone this repository
``git clone git@gitlab.com:bpm-dpex/dpex-build.git``

 2. Initialize and clone the git repositories specified as git submodules
 ``git submodule init``
``git submodule update --init --force --remote``

3. Build the docker image
``docker-compose build``

4. Run the docker image
``docker-compose up``

5. Access the application user-interface at ``http://localhost:3000``

## Submodules

 - Frontend [ ``dpex-react``](https://gitlab.com/bpm-dpex/dpex-react)
 - Backend [ ``dpex-demo``](https://gitlab.com/bpm-dpex/dpex-demo)
 - Matrix-Adapter [ ``dpex-communication-matrix-adapter``](https://gitlab.com/bpm-dpex/dpex-communication-matrix-adapter)
 - Discord-Adapter [``dpex-communication-discord-adapter``](https://gitlab.com/bpm-dpex/dpex-communication-discord-adapter)


## Docker

The docker compose file specifies one container for each git submodule and additionally a container for the ganache blockchain for testing and demonstration purposes.

 - Frontend
 - Backend
 - Matrix-Adapter
 - Discord-Adapter
 - Ganache

## Roadmap

Future releases will focus on bug fixing and flexibility regarding configuration of the whole application.